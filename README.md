# Node-Task

Markdown for node-task

## List of contents

Contents:

1. JavaScript function
2. Response image

JavaScript function ->

```
const add = (a, b) => {
    const sum = a + b;
    return sum;
};
```

Response ->
![responseimg](responsetime.png)